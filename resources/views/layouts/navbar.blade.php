<nav class="navbar navbar-expand-lg fixed-top" color-on-scroll="300">
    <div class="container">
        <div class="navbar-translate">
            <a class="navbar-brand" href="https://www.creative-tim.com">Alemannisch</a>
            <button class="navbar-toggler navbar-toggler-right navbar-burger" type="button" data-toggle="collapse"
                    data-target="#navbarToggler" aria-controls="navbarTogglerDemo02" aria-expanded="false"
                    aria-label="Toggle navigation">
                <span class="navbar-toggler-bar"></span>
                <span class="navbar-toggler-bar"></span>
                <span class="navbar-toggler-bar"></span>
            </button>
        </div>
        <div class="collapse navbar-collapse" id="navbarToggler">
            <ul class="navbar-nav ml-auto">
                <div class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" data-toggle="dropdown" id="dropdownMenuButton" href="#"
                       role="button" aria-haspopup="true" aria-expanded="false">Ejercicios</a>
                    <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                        {{--<li class="dropdown-header">Dropdown header</li>--}}
                        <a class="dropdown-item" href="/conjugador">Conjugador</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="#pk">Articulos</a>
                        <a class="dropdown-item" href="#pk">Verbos</a>
                        <a class="dropdown-item" href="#pk">Sustantivos</a>
                    </ul>
                </div>
                <li class="nav-item">
                    <a href="#" class="nav-link">{{--<i class="nc-icon nc-layout-11">--}}</i>Acceder</a>
                </li>
                {{--<li class="nav-item">
                    <a href="../documentation/tutorial-components.html" target="_blank" class="nav-link"><i
                                class="nc-icon nc-book-bookmark"></i> Documentation</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" rel="tooltip" title="Follow us on Twitter" data-placement="bottom"
                       href="https://twitter.com/CreativeTim" target="_blank">
                        <i class="fa fa-twitter"></i>
                        <p class="d-lg-none">Twitter</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" rel="tooltip" title="Like us on Facebook" data-placement="bottom"
                       href="https://www.facebook.com/CreativeTim" target="_blank">
                        <i class="fa fa-facebook-square"></i>
                        <p class="d-lg-none">Facebook</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" rel="tooltip" title="Follow us on Instagram" data-placement="bottom"
                       href="https://www.instagram.com/CreativeTimOfficial" target="_blank">
                        <i class="fa fa-instagram"></i>
                        <p class="d-lg-none">Instagram</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" rel="tooltip" title="Star on GitHub" data-placement="bottom"
                       href="https://www.github.com/CreativeTimOfficial" target="_blank">
                        <i class="fa fa-github"></i>
                        <p class="d-lg-none">GitHub</p>
                    </a>
                </li>--}}
            </ul>
        </div>
    </div>
</nav>