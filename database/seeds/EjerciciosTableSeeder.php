<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class EjerciciosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $ejercicios = [
            [
                'ejercicios_categorias_id' => 1,
                'nombre' => 'Pronombres Nominativos',
            ],
            [
                'ejercicios_categorias_id' => 1,
                'nombre' => 'Pronombres Acusativos',
            ],
            [
                'ejercicios_categorias_id' => 1,
                'nombre' => 'Pronombres Dativos',
            ],
            [
                'ejercicios_categorias_id' => 1,
                'nombre' => 'Pronombres Posesivos',
            ],
            [
                'ejercicios_categorias_id' => 1,
                'nombre' => 'Pronombres Reflexivos',
            ],
            [
                'ejercicios_categorias_id' => 2,
                'nombre' => 'Verbos Regulares',
            ],
            [
                'ejercicios_categorias_id' => 2,
                'nombre' => 'Verbos Irregulares',
            ],
            [
                'ejercicios_categorias_id' => 2,
                'nombre' => 'Verbos Modales',
            ],
        ];

        DB::table('ejercicios')->insert($ejercicios);
    }
}
