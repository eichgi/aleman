<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class VerbosNivelesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $niveles = [
            ['nivel' => 'basico'],
            ['nivel' => 'intermedio'],
            ['nivel' => 'avanzado'],
        ];

        DB::table('verbos_niveles')->insert($niveles);
    }
}
