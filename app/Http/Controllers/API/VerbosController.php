<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class VerbosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($categoria, $nivel)
    {
        /*$verbos = DB::table('verbos')
            ->join('verbos_categorias', 'verbos_categorias.id', '=', 'verbo_categoria_id')
            ->join('verbos_tiempos', 'verbos_tiempos.id', '=', 'verbo_tiempo_id')
            ->join('verbos_niveles', 'verbos_niveles.id', '=', 'verbo_nivel_id')
            ->where('categoria', $categoria)
            ->where('tiempo', $tiempo)
            ->where('nivel', $nivel)
            ->select('verbo', 'verb', 'ejemplo', 'exampel')
            ->get();*/

        $verbos = DB::table('verbos')
            ->join('verbos_categorias', 'verbos_categorias.id', '=', 'verbo_categoria_id')
            ->where('categoria', $categoria)
            ->where('nivel', $nivel)
            ->select('verbo', 'verb', 'ejemplo', 'exampel')
            ->get();

        $sql = DB::select('SELECT ejercicio_id 
                                    FROM verbos_categorias
                                    WHERE categoria = ?', [ucfirst($categoria)]);
        $ejercicio_id = $sql[0]->ejercicio_id;

        return response()->json(compact('verbos', 'ejercicio_id'), 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function niveles($categoria)
    {
        $niveles = DB::table('verbos')
            ->join('verbos_categorias', 'verbos_categorias.id', '=', 'verbo_categoria_id')
            ->where('categoria', ucfirst($categoria))
            ->max('nivel');

        return response()->json(compact('niveles'), 200);
    }
}
